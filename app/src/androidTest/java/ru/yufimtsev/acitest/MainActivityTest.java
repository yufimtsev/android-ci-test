package ru.yufimtsev.acitest;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.TextView;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mFirstTestActivity;
    private TextView mFirstTestText;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mFirstTestActivity = getActivity();
        mFirstTestText = (TextView) mFirstTestActivity.findViewById(R.id.text_view);
    }

    @MediumTest
    public void testInitialState() {
        final String expected = mFirstTestActivity.getString(R.string.hello_message);
        final String actual = mFirstTestText.getText().toString();
        assertEquals(expected, actual);
    }

    @MediumTest
    public void testClicked() {
        TouchUtils.clickView(this, mFirstTestText);
        assertEquals("CLICKED", mFirstTestText.getText().toString());
    }
}
